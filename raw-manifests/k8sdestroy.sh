# aws eks --region us-east-1 update-kubeconfig --name eks4api-demo
kubectl delete -f aws-auth.yaml
kubectl delete -f pod.yaml
kubectl delete -f service.yaml
echo -e "\n\n::::::: Pods ::::::::::::::::\n"
kubectl get pods
echo -e "\n\n::::::: Services ::::::::::::::::\n"
kubectl get services
