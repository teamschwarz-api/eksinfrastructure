# Create
aws eks --region us-east-1 update-kubeconfig --name eks4api-1120
kubectl apply -f aws-auth.yaml
kubectl apply -f pod.yaml
kubectl apply -f service.yaml

# Test
echo -e "\n\n::::::: Services ::::::::::::::::\n"
kubectl get services
echo -e "\n\n::::::: Pods ::::::::::::::::\n"
kubectl get pods
echo -e "\n\n::::::: Pod: api-test ::::::::::::::::\n"
kubectl describe pod api-test
echo -e "\n\n::::::: Service: api-test-svc ::::::::::::::::\n"
kubectl describe service api-test-svc