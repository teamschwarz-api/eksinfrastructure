variable "profile" {
  description = "AWS profile"
  default = ""
  type = string
}

variable "region" {
  description = "AWS region to deploy to"
  default = "us-east-1"
  type        = string
}

variable "cluster_name" {
  description = "EKS cluster name"
  default = "eks4api-1120"
  type = string
}