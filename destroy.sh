#!/bin/bash
#############################################################################
#
#   this script terraforms an eks cluster, creates the services and pod 
#   (via kubectl), then runs some tests to visually validate things were created.
#   
#############################################################################
clear
START_TIME=`date +%H":"%M":"%S`

cd raw-manifests
./k8sdestroy.sh
cd ..
terraform destroy -auto-approve 


STOP_TIME=`date +%H":"%M":"%S`
echo ""
echo "START_TIME: "$START_TIME
echo "STOP_TIME:  "$STOP_TIME
echo ""